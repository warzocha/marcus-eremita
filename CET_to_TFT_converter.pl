#!/usr/bin/perl
use strict;
use warnings;
use File::Slurp;

# CET_to_TFT_converter
# Program converts the CET (Critical Edition Text) files to TFT (Tools-Friendly Text) files.

sub convert_CET_to_TFT {
	my $txt = $_[0];
	$txt =~ s/\n\((\d+)\.?\)/\r\[$1\]/g;
	$txt =~ s/\s*\(\d+t?\)//g;
	$txt =~ s/\s*@\d+//g;
	$txt =~ s/-\n//g;
	$txt =~ s/\n/ /g;
	$txt =~ s/\s{5,}/\r/g;
	$txt =~ s/\s{2,}/ /g;
	return $txt;
}


if	( $ARGV[0] eq '-f' ) { # 'File mode' - program operates on a given file
	my $file = $ARGV[1];
	my $txt =  read_file( $file );
	$txt = convert_CET_to_TFT( $txt );
	write_file( $file, $txt );
}
elsif ( $ARGV[0] eq '-d' ) { # 'Directory mode' - program searches for .txt files in a given directory and operates on them
	my $dir = $ARGV[1];
	chdir ( $dir );
	my @files = read_dir( $dir );
	foreach my $file ( @files ) {
		if ( -T $file ) {
			my $txt = read_file( $file );
			$txt = convert_CET_to_TFT( $txt );
			write_file( $file, $txt);
		}
	}
}
elsif ( $ARGV[0] eq '--help' ) {
print <<'END';
CET to TFT Converter

This program converts the CET (Critical Edition Text) files to TFT (Tools-Friendly Text) files.

SYNTAX

CET_to_TFT_converter [OPTIONS] FILE

OPTIONS
	
--help	Print this instruction.

-f	Program operates on a given file.

-d	Program searches for .txt files in a given directory
	and operates on them.
END
}
else {
	print 'You should use -f,-d or --help switch.'
}